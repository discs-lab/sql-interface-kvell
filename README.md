# SQL Interface KVell

Author: Si Yi Li | MSc Project

# Require softwares for KVell
Install the following softwares using `apt-get`
* make 
* clang
* autoconf
* libtool

# Setup locally
To run the SQL interface locally, run the following commands in terminal
```
git clone git@gitlab.cs.mcgill.ca:discs-lab/sql-interface-kvell.git
mkdir ./scratch0/kvell
./scripts/tutorial.sh
```

Your terminal should display the following message
```
Please enter your SQL query
```

You can choose to enter one of the following SQL queries
```
1. SELECT l_ORDERKEY , o_ORDERDATE , o_SHIPPRIORITY , SUM(l_TAX) AS revenue FROM lineitem , orders , customer WHERE o_ORDERKEY = l_ORDERKEY AND o_CUSTKEY = c_CUSTKEY AND l_SHIPDATE > 2021-09-03 AND o_ORDERDATE < 2021-11-05 GROUP BY l_ORDERKEY , o_ORDERDATE , o_SHIPPRIORITY ORDER BY revenue DESC , o_ORDERDATE
2. SELECT c_CUSTKEY , c_NAME , c_ADDRESS , c_PHONE , n_NATIONNAME , SUM(l_EXTENDEDPRICE) AS revenue FROM customer , orders , lineitem , nation WHERE c_CUSTKEY = o_CUSTKEY AND o_ORDERKEY = l_ORDERKEY AND o_ORDERDATE >= 2021-03-01 AND o_ORDERDATE < 2021-06-01 AND l_RETURNFLAG = R AND c_NATIONKEY = n_NATIONKEY GROUP BY c_CUSTKEY , c_NAME , c_PHONE , c_ADDRESS , n_NATIONNAME ORDER BY revenue DESC
3. SELECT n_NATIONKEY , n_NATIONNAME , SUM(l_EXTENDEDPRICE) AS revenue FROM customer , orders , lineitem , nation , region WHERE c_CUSTKEY = o_CUSTKEY AND o_ORDERKEY = l_ORDERKEY AND o_ORDERDATE >= 2021-03-01 AND o_ORDERDATE < 2021-06-01 AND c_NATIONKEY = n_NATIONKEY AND n_REGIONKEY = r_REGIONKEY AND r_REGIONNAME = Europe GROUP BY n_NATIONNAME ORDER BY revenue DESC
```

# Result of the tests
## Result for query 1
```
ORDERKEY: 1, ORDERDATE: 2021-06-16, SHIPPRIORITY: 3, revenue: 128, 
ORDERKEY: 4, ORDERDATE: 2021-01-01, SHIPPRIORITY: 1, revenue: 100, 
ORDERKEY: 8, ORDERDATE: 2021-05-05, SHIPPRIORITY: 5, revenue: 86, 
ORDERKEY: 9, ORDERDATE: 2021-06-16, SHIPPRIORITY: 3, revenue: 86, 
ORDERKEY: 7, ORDERDATE: 2021-04-04, SHIPPRIORITY: 4, revenue: 83, 
ORDERKEY: 0, ORDERDATE: 2021-05-05, SHIPPRIORITY: 1, revenue: 80, 
ORDERKEY: 5, ORDERDATE: 2021-02-02, SHIPPRIORITY: 4, revenue: 75, 
ORDERKEY: 6, ORDERDATE: 2021-03-03, SHIPPRIORITY: 4, revenue: 58, 
ORDERKEY: 2, ORDERDATE: 2021-07-17, SHIPPRIORITY: 2, revenue: 52, 
ORDERKEY: 3, ORDERDATE: 2021-08-08, SHIPPRIORITY: 2, revenue: 36,
```

## Result for query 2
```
CUSTKEY: 10, NAME: Rob Meunier, ADDRESS: 2707 Webster Street, PHONE: (780)585-6680, NATIONNAME: France, revenue: 6891, 
CUSTKEY: 17, NAME: Margaret Wu, ADDRESS: 3353 Church Street, PHONE: (867)580-5248, NATIONNAME: China, revenue: 5774, 
CUSTKEY: 3, NAME: Ray Audet, ADDRESS: 1085 Par Drive, PHONE: (226)203-7946, NATIONNAME: Canada, revenue: 2252, 
```

## Result for query 3
```
NATIONKEY: FR, NATIONNAME: France, revenue: 20029, 
```
